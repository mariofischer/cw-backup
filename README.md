# cw-backup #

A small bash script to backup several remote hosts via rsync or lftp.

The script runs on OSX, but it should be easy to adapt it to any Unix platform by replacing the "osascript"-calls with something else.

**Please note**: This is not a backup solution which works out of the box, but an example script you can modify to fit your own needs. Bash scripting knowledge is required.

### Usage ###

* Each backup job is defined by a bash function whose name starts with "backup…"
* To run a single backup job: `cw-backup.sh backupSomesite`
* To run all backup jobs: `cw-backup.sh all`
* The backups are saved in subfolders of the folder given by `$backupDir`

### More information ###

See https://www.chipwreck.de/blog/2022/01/07/backup-remote-servers-via-ftp-rsync/
