#!/bin/bash
#
# Cw Backup Tool
#
## by Mario Fischer <mario@chipwreck.de>
## Version 1.4.1
## http://www.chipwreck.de
#
# Requires: rsync, lftp, osascript

####################################################
### Settings
####################################################

# backup root dir, must end with /
backupDir="$HOME/Projekte/_example-backup/"

# ltfp binary
lftpBin="/usr/local/bin/lftp"

# lftp mirror options
lftpMirrorOptions=" --verbose=1 --skip-noaccess --parallel=5 --delete --only-newer "

# lftp command start
lftpCmdStart="set ssl:verify-certificate false && set cache:enable true && set dns:cache-enable true && set ftp:use-feat off "

# rsync binary
rsyncBin="/usr/bin/rsync"

# rsync options
rsyncOptions=" -a -v --partial "

####################################################
### Backup methods - add your own methods here
####################################################

# backup via FTP - an example
#
backupMyblog()
{
	_lftpBkup \
	srcHost='www.myblog.example' \
	srcDirs='/downloads/,/data/' \
	opts='--exclude=cache/ --exclude-glob=*.zip' \
	targetDir='myblog'
}

# backup via rsync - an example
#
backupMyproject()
{
	_rsyncBkup \
	srcHost='user@myproject.example' \
	srcDirs='/var/www/myproject/data' \
	port='22' \
	opts='--exclude=**/.cache --exclude=*.gz' \
	targetDir='myproject'
}

####################################################
### Internal methods
####################################################

# Usage
#
usage()
{
	echo -e "\n${LGREEN}Cw Backup Tool${NC}\n"
	echo -e "$(cat $0|grep '^## .*')" # Read script itself (lines starting with '## ')
	echo -e "\nUsage: ${BOLD}${0} <action>${NC}"
	echo -e "\nPossible actions:\n"
	printf ' %s\n' "${commands[@]}"
	echo -e "\n"
	exit 1
}


# Backup all: Execute all methods whose name contains "backup"
#
all()
{
	echo '--------------------------------------------------'
	echo "$(date "+%Y-%m-%d %H:%M:%S") start backup jobs"
	echo '--------------------------------------------------'

	local totalSecs=0
	for job in "${commands[@]}"; do
		if [[ $job == *"backup"* ]]; then
			SECONDS=0
			echo "$(date "+%Y-%m-%d %H:%M:%S") start ${job}"
			${job}
			echo "$(date "+%Y-%m-%d %H:%M:%S") ${job} done in $SECONDS secs."
			(( totalSecs+=SECONDS ))
		fi
	done

	echo '--------------------------------------------------'
	echo "$(date "+%Y-%m-%d %H:%M:%S") backup jobs done in ${totalSecs} secs."
	echo '--------------------------------------------------'

	if [[ ${exitCode} -eq 0 ]]; then
	  _notify "Finished backup jobs (${totalSecs} secs)"
	else
	  _warn "Finished backup jobs (with errors) (${totalSecs} secs)"
	fi

	return "${exitCode-0}"
}

# Backup via rsync
# Parameters: srcHost, srcDirs (comma-separated), port, targetDir (optional, hostname is used if not set), opts (additional options, optional)
#
_rsyncBkup()
{
	while [[ $# -gt 0 ]]; do local "$1"; shift; done
	local targetDir="${backupDir}${targetDir-$srcHost}"
	local sshOption="ssh -p ${port}"
	IFS=',' read -ra srcDirs <<< "${srcDirs}"
	mkdir -p "${targetDir}"

	for srcDir in "${srcDirs[@]}"; do

    mkdir -p "${targetDir}${srcDir}"

    err=$("${rsyncBin}" ${rsyncOptions} ${opts} -e "${sshOption}" "${srcHost}:${srcDir}" "${targetDir}${srcDir}" 3>&1 1>&2 2>&3 | tee >(cat - >&2))

		[[ -n "${err}" ]] && _warn "rsync-backup failed with ${err} in ${srcDir}"

	done

	return "${exitCode-0}"
}

# Backup via lftp, username/passwords are taken from ~/.netrc
# Parameters: srcHost, srcDirs (comma-separated), targetDir (optional, hostname is used if not set), opts (additional options, optional)
#
_lftpBkup()
{
	while [[ $# -gt 0 ]]; do local "$1"; shift; done
	local targetDir="${backupDir}${targetDir-$srcHost}/"
	IFS=',' read -ra srcDirs <<< "${srcDirs}"
	mkdir -p "${targetDir}"

	local lftpCmd="${lftpCmdStart}"
	for srcDir in "${srcDirs[@]}"; do
	  mkdir -p "${targetDir}${srcDir}"
		lftpCmd+=" ; mirror ${lftpMirrorOptions} ${opts} ${srcDir} ${targetDir}${srcDir}"
	done
	lftpCmd+=" ; bye"

	err=$("${lftpBin}" "${srcHost}" -e "${lftpCmd}" 3>&1 1>&2 2>&3 | tee >(cat - >&2))

	[[ -n "${err}" ]] && _warn "lftp failed with ${err} for ${srcHost}"

	return "${exitCode-0}"
}

# Init, check binaries & target, set some variables
#
_init()
{
	[[ ! -x "${lftpBin}" ]] && _warn "lftp binary ${lftpBin} not found"
	[[ ! -x "${rsyncBin}" ]] && _alert "rsync binary ${rsyncBin} not found"
	[[ ! -d "${backupDir}" ]] && _alert "Backup folder ${backupDir} does not exist"

	commands=($(typeset -F | awk '/ [a-zA-Z]+$/{print $3}')) # find possible commands named [a-zA-Z]+
	interactive=0

	if [[ -t 1 ]]; then # we are on an interactive terminal
		interactive=1
    BOLD='\033[1m' # Escape sequences
		LRED='\033[1;31m'
		LGREEN='\033[1;32m'
		LBLUE='\033[0;34m'
		NC='\033[0m' # No color
	fi
}

# Notify: on screen and optionally via applescript
#
_notify()
{
	echo -e "\n${LGREEN}Success: $1 ${NC}\n"
	[[ ! ${interactive} ]] && osascript -e 'display notification "'"${1:0:1024}"'" with title "Cw Backup" sound name "Purr"' > /dev/null
}

# Show a warning, on screen and optionally via applescript
#
_warn()
{
	exitCode=1
	echo -e "Warning: ${LBLUE}${1}${NC}\n"
	[[ ! ${interactive} ]] && osascript -e 'display notification "'"${1:0:1024}"'" with title "Cw Backup" subtitle "Warning" sound name "Sosumi"' > /dev/null
}

# Alert and exit. always via applescript, /dev/null to discard button return value
#
_alert()
{
	echo -e "Error: ${LRED}${1}${NC}\n"
	osascript -e 'display alert "Error in Cw Backup" message "'"${1:0:1024}"'" as critical giving up after 60' > /dev/null
	exit 1
}

####################################################
### MAIN
####################################################

_init
[[ ! "$(type -t ${1})" = 'function' ]] && usage # not a valid command? show usage
$1 # execute command

exit $?
